const controllers = {
  reset: require("./reset"),
  auth: require("./auth"),
  experience: require("./experience"),
  middleware: require("./middleware"),
};

module.exports = controllers;
