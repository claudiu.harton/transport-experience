const express = require("express");
const router = express.Router();
const { experience, auth } = require("../controllers");

//experience
router.post(
    "/", experience.addExperience
);
router.delete(
    "/",
    experience.deleteExperience
);
router.put(
    "/",
    experience.editExperience
);
router.get(
    "/",
    experience.getExperiences
);

router.get(
    "/all",
    experience.getAllExperiences
);

module.exports = router;
